[kube_masters]
%{ for node in kube_masters ~}
${node.tags.Name} ansible_host=${node.public_ip}
%{ endfor ~}

[kube_workers]
%{ for node in kube_workers ~}
${node.tags.Name} ansible_host=${node.public_ip}
%{ endfor ~}

[vars:all]
ansible_python_interpreter=/usr/bin/python3