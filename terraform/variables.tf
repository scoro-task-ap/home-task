#Default AWS region details.
variable "aws_region" {
  description = "Default AWS region."
  type        = string
  default     = "eu-north-1"
}

#The number of EC2 instances to deploy for the workers.
variable "kube_worker_instances" {
  default = 3
}

#The number of EC2 instances to deploy for the master.
#For this setup. Leave this as 1.
#As there will be some updates needed in the ansible to do multi-master cluster setup.
variable "kube_master_instances" {
  default     = 1
}

#The EC2 instance size of the master
variable "kube_master_size" {
  default = "t3.small"
}

#The EC2 instance size of the worker
variable "kube_worker_size" {
  default = "t3.small"
}

#Allow All local and SSH traffic
#and in addition allow the range of ports for use as NodePorts in kubernetes
variable "sg_ingress_rules" {
  default = [
    {
      description = "Allow 22 from anywhere"
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      description = "Allow NodePort"
      from_port   = 30000
      to_port     = 30500
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      description = "Allow All local traffic"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["10.20.0.0/16"]
    }
  ]

}

#Allow all outgoing traffic
variable "sg_egress_rules" {
  default = [
    {
      description = "Allow all traffic to go out."
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
}