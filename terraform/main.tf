terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = var.aws_region
}

#Get all available AZ's in VPC
data "aws_availability_zones" "main_azs" {
  state = "available"
}

#Get AWS AMI for Ubuntu 22.04 in the region
#https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

#Create VPC
resource "aws_vpc" "main_vpc" {
  cidr_block = "10.20.0.0/16"
  enable_dns_hostnames = true
}

#Creates one subnet in the VPC which we created
#All our objects are deployed in this subnet. (We will make a Single AZ cluster)
resource "aws_subnet" "subnet_1" {
  availability_zone = element(data.aws_availability_zones.main_azs.names, 0)
  vpc_id            = aws_vpc.main_vpc.id
  cidr_block        = "10.20.30.0/24"
}

#Create an IGW and attaches it to the VPC created above
resource "aws_internet_gateway" "main_gw" {
  vpc_id = aws_vpc.main_vpc.id
}

#Create main route table and adds the global route to route using the IGW, 
#local routing as added by default.
resource "aws_default_route_table" "main_route_table" {
  default_route_table_id = aws_vpc.main_vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main_gw.id
  }
}

#Add a key-pair for logging into the EC2 instances
resource "aws_key_pair" "main_ssh_key" {
  key_name   = "main-access-key"
  public_key = file("~/.ssh/id_rsa.pub")
}

#Create a security grorup, the ingress and egress values come from the variables 
resource "aws_security_group" "main_sg" {
  name        = "main-sg"
  description = "Main security group. Managed my Terrafrom."
  vpc_id      = aws_vpc.main_vpc.id
  dynamic "ingress" {
    for_each = var.sg_ingress_rules
    content {
      description = ingress.value["description"]
      from_port   = ingress.value["from_port"]
      to_port     = ingress.value["to_port"]
      protocol    = ingress.value["protocol"]
      cidr_blocks = ingress.value["cidr_blocks"]
    }
  }
  dynamic "egress" {
    for_each = var.sg_egress_rules
    content {
      description = egress.value["description"]
      from_port   = egress.value["from_port"]
      to_port     = egress.value["to_port"]
      protocol    = egress.value["protocol"]
      cidr_blocks = egress.value["cidr_blocks"]
    }
  }

}

#Create 1 Kubernetes master server
resource "aws_instance" "kube_master" {
  count                       = var.kube_master_instances
  ami                         = data.aws_ami.ubuntu.id
  key_name                    = aws_key_pair.main_ssh_key.key_name
  security_groups             = [aws_security_group.main_sg.id]
  subnet_id                   = aws_subnet.subnet_1.id
  instance_type               = var.kube_master_size
  associate_public_ip_address = "true"
  root_block_device {
    volume_type           = "gp3"
    volume_size           = 20
    delete_on_termination = "true"
  }
  tags = {
    Name = "kube-master-${count.index + 1}"
  }
}

#Create 3 worker server
resource "aws_instance" "kube_worker" {
  count                       = var.kube_worker_instances
  ami                         = data.aws_ami.ubuntu.id
  key_name                    = aws_key_pair.main_ssh_key.key_name
  security_groups             = [aws_security_group.main_sg.id]
  subnet_id                   = aws_subnet.subnet_1.id
  instance_type               = var.kube_worker_size
  associate_public_ip_address = "true"
  root_block_device {
    volume_type           = "gp3"
    volume_size           = 20
    delete_on_termination = "true"
  }
  tags = {
    Name = "kube-worker-${count.index + 1}"
  }
}

#Generate the inventory file for Ansible, and place it in the ansible dir of this project
resource "local_file" "ansible_inventory" {
  content = templatefile("templates/hosts.tpl",
    {
      kube_masters = aws_instance.kube_master.*
      kube_workers = aws_instance.kube_worker.*
    }
  )
  filename = "../ansible/ansible_inventory"
}