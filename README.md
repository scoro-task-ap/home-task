# Scoro Home Task

## Overview 

- This mono repo project has three folders:
- Terraform - Which is used to deploy the infrastructure on AWS.
- Helm - Which is used to deploy JuiceShop application on the Kubernetes cluster.
- Ansible - Which is used  to set up the Kubernetes cluster, and install the JuiceShop Helm Chart on it.

> NB! This document is written keeping in mind that the readers are technical users.

## Requirements

System requrirements: 
- Have necessary executables for:
    - Ansible 5.x (ansible-core 2.12.5)
    - Helm v3.8.2
    - Terraform v1.1.9

## Things to do outside this repository
Make sure you have your ssh public key at `~/.ssh/id_rsa.pub`, if you have some other name or location, you could just copy it here for this task, or just generate a new temp one.
This is required as both Terraform and Ansible use this to grant access to the servers, and for this task, it was simpler to just set it to this file, rather than make it customisable. 

## Customising inside this repository
- Clone this repository to your system, and `cd` into it. 
- Customise all the variables as per your preference and requirements:
    - Helm variables are located at `scoro-juiceshop/values.yaml`
    - Terrafrom variables are at `terraform/variables.tf`
    - Ansible variables are at `ansible/group_vars/all/vars.yml`
- Once you are done with that, we can proceed to actually deploying the components.

## Deploying

There are four commands which are needed `helm package` , `terraform init`, `terraform apply`, and `ansible-playbook` 

Step 1: Let's package the Helm chart into a compressed file. While in the root of this project execute `helm package scoro-juiceshop/`
That will give an output like: 
> Successfully packaged chart and saved it to: .../home-task/scoro-juiceshop-x.x.x.tgz

> NB! If you update the release number, and the output file is not *-0.1.0.tgz, you would need to update the `helm_chart_file_name` variable in `ansible/group_vars/all/vars.yml`

Step 2: Let's deploy the infrastructure
- We need to sent the envoirnment variables for Terraform to access the AWS account.
    - To do that you can just set the below env vars or use a tool like the `aws` utility on your system to do it. 
    - Commands to set the env vars, `export AWS_ACCESS_KEY_ID=<your-key-id>` and  `export AWS_SECRET_ACCESS_KEY=<your-secret-key>`

> NB! This repository will not work on tools like localstack, due to the fact it queries the AWS region to get the ami for Ubuntu 22.04 in that region, which fails on localstack.

- Now lets initialise the terrafrom code by entering `terraform -chdir=terraform/ init`
- After the init is complete, we can test the config and visualise what resources will be deployed by executing a terraform plan cmmand, from the root of this project you can use the command `terraform -chdir=terraform/ plan`
- If everything is ok, we can apply this by using `terraform -chdir=terraform/ apply`, don't forget to enter `yes` when it asks to confirm this deployment, or add the `--auto-approve` flag to the command when running it.
> NB! If you `cd` into the terraform dir, then you don't need the `-chdir` flag.

Step 3: Let's configure the cluster, and deploy JuiceShop on it. 
- This is the simplest of all, there is one command which will do everything.
- `cd` into the `ansible` folder of this repository.
- Now all you need to do is run `ansible-playbook one-touch-setup.yml`
- Once this task runs successsfully, it will output URL's to access the JuiceShop instance. 

> NB! Please wait a couple of minutes after running the ansible, as it takes some time to download the Juice Shop image and run the pod.

---

## Customisation explained more throughly

- The deployment name, service name, and image are all part of the helm chart, and can be updated via the Helm variables at `scoro-juiceshop/values.yaml`
- In case you wish to update them and deploy, you can do that via ansible as well, follow the steps:
    - First, in the root of this project, you need to run `helm package scoro-juiceshop/` again. 
    - Second, you need to uninstall the current installation. You can do that by running `ansible-playbook uninstall-juice-shop.yml` in the ansible dir.
    - Third you install the chart, by running `ansible-playbook install-juice-shop.yml` in the ansible dir.
    
    > NB! Remember to update the `helm_chart_file_name` variable in ansible if you updated the release number for the Chart. 

## Describing my solution and steps for setting up Kubernetes cluster

I split the R&D into two parts, provisioning infrastructure and configuring it.

For provisioning I went with Terraform, it is a very basic script which creates the necessary VPC, EC2 instances, subnet, security group, IGW, route table, etc.

I had completed the theory part of Terraform, as I mentioned previously, and this was a simple enoght use case to demonstrait that learning. 

As expected it included a lot of looking up the documentation describing various provisioners etc, and quite a lot of trial and error initially, but eventually was solved. 

For configuring the servers/ creating the cluster, I used Ansible. 
Since I have been using it for a while, I already had some of the basic roles such as ssh, or users with me, but the others was a mix of reading documentation, guides, examples, and just translating the bash commands into ansible blocks in many cases. 

Helm was also interesting, since I deployed on EC2 instances rather than EKS or something similar, the ingress-proxy was a bit of a weird solution, as most of the solutions I found online were for EKS/GKE or using Minikuke. 

I ended up with using the bare metal deploy script for the nginx-ingress, and updated the nodePort value to 30000 and 305000 for HTTP and HTTPS respectively, so that I could display an output in ansible as to where the Juice Shop instance will be availaible from. 

Also, in the ingress defined the host as the /Master node's IP/.nip.io 
In this case not All the traffic will be sent to the Juice Shop service.

Overall since I had a base understanding in all the technologies used in this project, I could just research for answers to probelms or read the documentation to create a solution. 

Quite a fun task to do!